#!/usr/bin/python3
from io import BytesIO
import os
import sys
from lintian_brush.control import (
    ensure_minimum_version,
    update_control,
    )

minimum_version = "9.20160114"


def bump_debhelper(control):
    control["Build-Depends"] = ensure_minimum_version(
            control["Build-Depends"],
            "debhelper",
            minimum_version)


dbg_packages = set()
dbg_migration_done = set()


def del_dbg(control):
    # Delete the freeradius-dbg package from debian/control
    package = control["Package"]
    if package.endswith('-dbg'):
        if package.startswith('python'):
            # -dbgsym packages don't include _d.so files for the python
            # interpreter
            return
        dbg_packages.add(control["Package"])
        control.clear()


update_control(source_package_cb=bump_debhelper, binary_package_cb=del_dbg)

current_version = os.environ["CURRENT_VERSION"]
migrate_version = "<< %s%s" % (
        current_version,
        '' if current_version.endswith('~') else '~')

outf = BytesIO()
with open('debian/rules', 'rb') as f:
    for line in f:
        if line.strip() == b"include /usr/share/cdbs/1/rules/debhelper.mk":
            # Ah, cdbs.
            raise Exception("package uses cdbs")
        if line.startswith(b'\tdh_strip '):
            for dbg_pkg in dbg_packages:
                if ('--dbg-package=%s' % dbg_pkg).encode('utf-8') in line:
                    line = line.replace(
                            ('--dbg-package=%s' % dbg_pkg).encode('utf-8'),
                            ("--dbgsym-migration='%s (%s)'" % (
                                dbg_pkg, migrate_version)).encode('utf-8'))
                    dbg_migration_done.add(dbg_pkg)
        outf.write(line)

if not dbg_packages:
    # no debug packages found to remove
    sys.exit(2)

difference = dbg_packages.symmetric_difference(dbg_migration_done)

if difference:
    raise Exception("packages missing %r" % difference)

with open('debian/rules', 'wb') as f:
    f.write(outf.getvalue())

print("Transition to automatic debug package%s (from: %s)." %
      (("s" if len(dbg_packages) > 1 else ""), ', '.join(dbg_packages)))
print("Fixed-Lintian-Tags: debian-control-has-obsolete-dbg-package")
